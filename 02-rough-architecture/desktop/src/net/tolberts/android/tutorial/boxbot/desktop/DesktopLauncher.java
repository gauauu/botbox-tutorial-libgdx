package net.tolberts.android.tutorial.boxbot.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.tolberts.android.tutorial.boxbot.BoxBotGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        //here we just changed BoxBox() to BoxBotGame(),
        //to use our new class that extended Game
		new LwjglApplication(new BoxBotGame(), config);
	}
}
