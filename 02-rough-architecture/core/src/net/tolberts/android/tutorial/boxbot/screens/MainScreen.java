package net.tolberts.android.tutorial.boxbot.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

//ScreenAdapter is just an abstract class that implements Screen
//with a bunch of empty methods, making it cleaner to look at.
public class MainScreen extends ScreenAdapter {

    private final SpriteBatch batch;
    private Texture img;

    //For now, we assume that the game class will pass in the image to draw.
    //This will change as we get more complicated and have a lot of
    //actual objects to draw.
    public MainScreen(Texture img) {
        this.img = img;

        //we still need a spritebatch to wrap our drawing routines in.
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        //delta is provided by libGDX, and is the amount of time that has passed (in seconds)
        //since render() was last called. This will be used later to make sure our game runs at a proper speed.
        //See 03-moving-and-updating


        //this is all just example code from the setup script:


        //first clear the screen to red
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //then draw our image in the bottom right
        batch.begin();
        batch.draw(img, 0, 0);
        batch.end();
    }
}