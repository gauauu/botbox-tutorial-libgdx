package net.tolberts.android.tutorial.boxbot;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import net.tolberts.android.tutorial.boxbot.screens.MainScreen;


public class BoxBotGame extends Game {
    @Override
    public void create() {

        //Create the texture from the image file, like we did before.
        //Eventually we'll move this into a separate class to load our
        //texure resources, but for now, this is fine.
        Texture img = new Texture("badlogic.jpg");

        //Instantiate our main screen, and hand it the texture we just loaded
        Screen mainScreen = new MainScreen(img);

        //tell the game class that the screen we want to be calling render()
        //on is our new mainScreen.
        setScreen(mainScreen);


    }
}
