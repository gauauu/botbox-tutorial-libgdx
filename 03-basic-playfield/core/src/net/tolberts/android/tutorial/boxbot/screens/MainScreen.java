package net.tolberts.android.tutorial.boxbot.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.tolberts.android.tutorial.boxbot.BoxBotGame;
import net.tolberts.android.tutorial.boxbot.playfield.PlayField;

//ScreenAdapter is just an abstract class that implements Screen
//with a bunch of empty methods, making it cleaner to look at.
public class MainScreen extends ScreenAdapter {

    private final SpriteBatch batch;
    private final PlayField playField;
    private Texture img;
    private OrthographicCamera camera;

    //For now, we assume that the game class will pass in the image to draw.
    //This will change as we get more complicated and have a lot of
    //actual objects to draw.
    public MainScreen(Texture img) {
        this.img = img;

        //we still need a spritebatch to wrap our drawing routines in.
        batch = new SpriteBatch();

        //create the camera and look at an area bounded by the WIDTH and HEIGHT we defined
        camera = new OrthographicCamera();
        camera.setToOrtho(false, BoxBotGame.WIDTH , BoxBotGame.HEIGHT);

        playField = new PlayField();
    }

    @Override
    public void render(float delta) {
        //delta is provided by libGDX, and is the amount of time that has passed (in seconds)
        //since render() was last called. This will be used later to make sure our game runs at a proper speed.
        //See 03-moving-and-updating


        camera.update();

        //first clear the screen to the background color.
        Gdx.gl.glClearColor(0.49f, 0.596f, 0.286f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //use the coordinate system defined by the camera
        batch.setProjectionMatrix(camera.combined);
        //then draw our image in the bottom right
        batch.begin();
        playField.render(batch);
        batch.end();
    }
}