package net.tolberts.android.tutorial.boxbot.playfield;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Platform {

    public Platform(float x, float y, float width, float height, Texture image) {
        bounds = new Rectangle(x, y, width, height);
        this.image = image;
    }

    //the bounds of the platform are defined by a rectangle.
    Rectangle bounds;

    //for a larger project, it would be good practice to separate the
    //game logic from the rendering details. Instead of storing
    //the image for the platform as a Texture, it might be better to
    //have an identifier instead, that the rendering code could use
    //to look up the texture. But once again, here we're keeping it simple.
    Texture image;


    public void render(SpriteBatch batch) {
        batch.draw(image, bounds.x, bounds.y);
    }
}
