package net.tolberts.android.tutorial.boxbot.background;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.tolberts.android.tutorial.boxbot.BoxBotGame;
import net.tolberts.android.tutorial.boxbot.Renderable;
import net.tolberts.android.tutorial.boxbot.Updateable;
import net.tolberts.android.tutorial.boxbot.assets.Textures;

import java.util.ArrayList;

//This shares a lot of similar functionality with PlayField
//(Basically a big list of Renderables. This would be a good
//candidate for a better design to prevent repeated similar code.
//We'll leave it here to make the tutorial game easier to understand)
public class Background implements Renderable, Updateable{
    private static final float ROT_SPEED = 2000f;
    ArrayList<Gear> gears;

    Texture backdrop;

    Texture light1;
    Texture light2;
    Texture light3;

    public Background() {
        gears = new ArrayList<Gear>(13);

        //load the backdrop
        backdrop = Textures.getTexture("backdrop");

        //and the light rays
        light1 = Textures.getTexture("obj_lightrays1");
        light2 = Textures.getTexture("obj_lightrays2");
        light3 = Textures.getTexture("obj_lightrays3");

        //since we're only referring to these things by number anyway,
        //might as well put them in an array and make it easier.
        Texture[] textures = new Texture[]{
          Textures.getTexture("obj_cog1"),
          Textures.getTexture("obj_cog2"),
          Textures.getTexture("obj_cog3"),
          Textures.getTexture("obj_cog4"),
          Textures.getTexture("obj_cog5"),
          Textures.getTexture("obj_cog6"),
        };

        //Start adding the gears.
        //Again, if this was a real game, I wouldn't hardcode these.
        //The reason we alternate between positive and negative ROT_SPEED is
        //so that the gears turn in alternating directions, to give the illusion of them
        //turning each other.
        gears.add(new Gear(150, 212, textures[0], ROT_SPEED));
        gears.add(new Gear(91, 281, textures[1], -ROT_SPEED));
        gears.add(new Gear(149, 382, textures[3], ROT_SPEED));
        gears.add(new Gear(286, 436, textures[4], -ROT_SPEED));
        gears.add(new Gear(405, 310, textures[5], ROT_SPEED));
        gears.add(new Gear(198, 525, textures[2], ROT_SPEED));


        gears.add(new Gear(465, 635, textures[0], ROT_SPEED));
        gears.add(new Gear(579, 546, textures[5], -ROT_SPEED));
        gears.add(new Gear(744, 539, textures[3], ROT_SPEED));
        gears.add(new Gear(779, 682, textures[4], -ROT_SPEED));

        gears.add(new Gear(1002, 398, textures[1], ROT_SPEED));
        gears.add(new Gear(945, 260, textures[5], -ROT_SPEED));
        gears.add(new Gear(832, 132, textures[4], ROT_SPEED));

    }

    public void update(float delta) {
        //update all the gears
        for (Gear gear : gears) {
            gear.update(delta);
        }
    }

    public void render(SpriteBatch batch) {

        batch.draw(backdrop, 0, 0);

        batch.draw(light1, 150, 100);
        batch.draw(light2, 580, 125);
        //this is a lot of numbers and looks intimidating, but it's just a lot more details
        //that you can specify for the draw command. The important part is the 20.0f, which
        //sets the rotation. The rest is just drawing it at the regular size. (see
        //the comments in Gear.java for the full description of all these parameters)
        batch.draw(light3, 950, 150, 0, 0, 160, 640, 1.0f, 1.0f, 20.0f, 0, 0, 160, 640, false, false);

        //render all the gears
        for (Gear gear : gears) {
            gear.render(batch);
        }

    }
}
