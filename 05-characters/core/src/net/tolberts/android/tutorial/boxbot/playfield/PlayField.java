package net.tolberts.android.tutorial.boxbot.playfield;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.tolberts.android.tutorial.boxbot.Renderable;
import net.tolberts.android.tutorial.boxbot.assets.Textures;

import java.util.ArrayList;

/**
 * The playfield class manages the platforms/etc....
 * Any "background" elements that are actually an interactive part of the game.
 */
public class PlayField implements Renderable {

    ArrayList<Platform> platforms;

    public static final float PLATFORM_WIDTH_BIG = 144;
    public static final float PLATFORM_WIDTH_SMALL = 64;

    public static final float PLATFORM_HEIGHT = 64;

    public PlayField() {
        platforms = new ArrayList<Platform>(13);

        //load the textures for the different platforms
        Texture platformSmall = Textures.getTexture("obj_platform1");
        Texture platformBig = Textures.getTexture("obj_platform2");
        Texture platformStand = Textures.getTexture("obj_platform2_stand");
        Texture platformGroundTop = Textures.getTexture("obj_platform_ground_top");
        Texture platformGroundBottom = Textures.getTexture("obj_platform_ground_bottom");


        //in a more complicated game, you'd initialize the platform data from some sort of
        //data file, (likely managed by a separate tool) instead of hardcoding it....

        //top big platforms
        platforms.add(new Platform(158, 525, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));
        platforms.add(new Platform(720, 525, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));

        //middle big platforms
        platforms.add(new Platform(0, 394, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));
        platforms.add(new Platform(880, 394, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));

        //bottom big platforms
        platforms.add(new Platform(158, 259, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));
        platforms.add(new Platform(440, 259, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));
        platforms.add(new Platform(720, 259, PLATFORM_WIDTH_BIG, PLATFORM_HEIGHT, platformBig));

        //middle small platforms
        platforms.add(new Platform(355, 394, PLATFORM_WIDTH_SMALL, PLATFORM_HEIGHT, platformSmall));
        platforms.add(new Platform(480, 394, PLATFORM_WIDTH_SMALL, PLATFORM_HEIGHT, platformSmall));
        platforms.add(new Platform(605, 394, PLATFORM_WIDTH_SMALL, PLATFORM_HEIGHT, platformSmall));

        //make the stand
        platforms.add(new Platform(480, 181, PLATFORM_WIDTH_SMALL, 80, platformStand));

        //make the ground in 2 parts, so we have 2 distinct rectangles for collisions later
        platforms.add(new Platform(305, 122, 424, 59, platformGroundTop));
        platforms.add(new Platform(0, 0, 1024, 121, platformGroundBottom));


    }

    public void render(SpriteBatch batch) {
        for (Platform platform : platforms) {
            platform.render(batch);
        }

    }


}
