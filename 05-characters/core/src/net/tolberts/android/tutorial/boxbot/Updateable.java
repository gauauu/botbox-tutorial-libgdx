package net.tolberts.android.tutorial.boxbot;

/**
 * Any item that can be updated per frame
 */
public interface Updateable {
    public void update(float delta);
}
