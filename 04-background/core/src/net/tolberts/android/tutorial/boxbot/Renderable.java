package net.tolberts.android.tutorial.boxbot;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * This is just any item than can be rendered via a spriteBatch
 */
public interface Renderable {
    public void render(SpriteBatch batch);
}
