package net.tolberts.android.tutorial.boxbot.background;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.tutorial.boxbot.Renderable;
import net.tolberts.android.tutorial.boxbot.Updateable;

public class Gear implements Renderable, Updateable{

    final Vector2 center;
    final Texture image;
    float angle;
    final float rotationalSpeed;

    public Gear(float x, float y, Texture image, float rotationalSpeed) {
        //because gears spin around their center, we'll track everything
        //by the center instead of a corner
        center = new Vector2(x, y);
        this.image = image;

        //the angle is the current rotation to draw the gear
        this.angle = 0;

        //rotation speed lets us change the speed at which it rotates
        //the gears should turn at different rates inversely proportional to their
        //radius, to create the illusion that they are actually turning each other.
        //Why minus 32? With gears, you measure the distince from center to the
        //pitch circle, not the outer edge of the teeth. The gear teeth are 16px long
        //in our images, so we remove 32 pixels (for teeth on both sides) to get the
        //size of the pitch circle.
        this.rotationalSpeed = rotationalSpeed * (1.0f / (image.getWidth() - 32));

    }


    //Because the gear changes over time,
    //we call update each frame and update based on the time delta
    //since the last frame
    public void update(float delta) {

        //rotationalSpeed is the angle change per second. Delta is the number
        //of seconds that have passed since the last frame.
        angle += delta * rotationalSpeed;

        //once we get all the way around, reset back to 0
        //so we don't eventually overflow
        if (angle >= 360) {
            angle -= 360.0f;
        }
    }


    public void render(SpriteBatch batch) {

        //set the alpha blending of the next draw to 0.4f, so the gears are faded.
        //(the first 3 numbers are the rgb values, which are white for a normal
        //color tint)
        batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);


        //batch.draw has a number of different overloaded signatures..
        //this one is complicated, but lets us set an arbitrary rotation
        //around an arbitrary point (we use the center)
        batch.draw(image,
                center.x - image.getWidth() / 2,    //draw the corner at center - width / 2
                center.y - image.getHeight() / 2,   //(same with height)

                image.getWidth() / 2,           //these parameters specify the origin
                image.getHeight() / 2,          //for scaling/rotation

                image.getWidth(),               //target height/width for drawing
                image.getHeight(),

                1.0f, 1.0f,                     //scaling. We just draw at 100% scale

                angle,                          //here we specify the rotation

                0, 0,

                image.getWidth(), image.getHeight(), false, false);

        //reset the alpha to normal
        batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

}
