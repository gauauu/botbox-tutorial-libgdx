package net.tolberts.android.tutorial.boxbot.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;
import java.util.Map;

/**
 * This is really just a wrapper so we can later change how we're
 * loading textures without having to change lots of places throughout the code.
 * There are a number of strategies for loading textures
 * (synchronously vs asynchronously, as individual files vs packed into a single file, etc).
 * This gives us flexibility to change our strategy without touching everything.
 */
public class Textures {

    //cache the textures we've already loaded, so we can just ask for them
    //by name and not worry about accidentally re-loading them.
    private static Map<String, Texture> textureCache = new HashMap<String, Texture>();

    //if we don't have the texture already cached, load it in the simplest possible
    //mechanism (for now at least!)
    public static Texture getTexture(String imageName) {

        if (!textureCache.containsKey(imageName)) {
            Texture texture = new Texture(Gdx.files.internal("data/textures/"
                    + imageName + ".png"));
            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            textureCache.put(imageName, texture);

        }
        return textureCache.get(imageName);

    }

}
