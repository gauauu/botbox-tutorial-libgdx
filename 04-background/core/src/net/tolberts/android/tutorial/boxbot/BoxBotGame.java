package net.tolberts.android.tutorial.boxbot;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import net.tolberts.android.tutorial.boxbot.screens.MainScreen;


public class BoxBotGame extends Game {

    //There are various strategies to handle the different possible screen sizes.
    //For the sake of simplicity on this game, we'll assume 1024x768 virtual pixels,
    //and let libGDX scale it to whatever actual screen size we're on.
    //These constants aren't used directly by libGDX, but our game uses
    //them to set the camera viewport, etc.
    public static float WIDTH = 1024;
    public static float HEIGHT = 768;

    public void create() {

        //Instantiate our main screen, and hand it the texture we just loaded
        Screen mainScreen = new MainScreen();

        //tell the game class that the screen we want to be calling render()
        //on is our new mainScreen.
        setScreen(mainScreen);


    }
}
